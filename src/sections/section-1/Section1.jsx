import React from "react";
import { Container } from "react-bootstrap";
import { Element } from "react-scroll";
import "./section-1.scss";

const Section1 = () => {
  return (
    <Element
      name="section-1"
      className="section-1"
      style={{ overflow: "hidden" }}
    >
      <Container>Section1</Container>
    </Element>
  );
};

export default Section1;
