import React from "react";
import { Container } from "react-bootstrap";
import { Element } from "react-scroll";
import "./section-2.scss";

const Section2 = () => {
  return (
    <Element
      name="section-2"
      className="section-2"
      style={{ overflow: "hidden" }}
    >
      <Container>Section 2</Container>
    </Element>
  );
};

export default Section2;
