import React from "react";
import "./hamburger-icon.scss";

const HamburgerIcon = ({ menu_show, toggleMenu }) => {
  return (
    <a
      onClick={toggleMenu}
      class={`nohighlight ${menu_show ? "active" : ""}`}
      id="burger-icon"
      href="#"
    >
      <span class="burger part-1"></span>
      <span class="burger part-2"></span>
      <span class="burger part-3"></span>
    </a>
  );
};

export default HamburgerIcon;
