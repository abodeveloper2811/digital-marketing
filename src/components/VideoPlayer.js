import React, {useState, useRef} from 'react';
import YouTube from 'react-youtube';

const VideoPlayer = () => {
    const videoIds = 'ywAZEtZe';
    const [currentVideoIndex, setCurrentVideoIndex] = useState(0);
    const playerRef = useRef(null);

    const opts = {
        playerVars: {
            autoplay: 1,
        },
    };

    const onEnd = () => {
        // Increment the index to play the next video in the list
        setCurrentVideoIndex((prevIndex) => (prevIndex + 1) % videoIds.length);
    };

    const onReady = (event) => {
        event.target.playVideo();
    };

    return (
        <YouTube
            videoId={videoIds}
            opts={opts}
            onReady={onReady}
            onEnd={onEnd}
            ref={playerRef}
        />
    );
};

export default VideoPlayer;